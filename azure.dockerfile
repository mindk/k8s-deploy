FROM alpine:3.12.0

RUN set -x && \
    apk add --no-cache --virtual .build-deps \
        gcc \
        make \
        libffi-dev \
        openssl-dev \
        musl-dev \
        python3-dev && \
    apk add --no-cache \
        py3-pip \
        python3 \
        curl \
        pwgen \
	mysql-client \
	mongodb-tools \
        bash \
        coreutils && \
    apk del .build-deps && \
    curl -fsSLO https://storage.googleapis.com/kubernetes-release/release/v1.18.2/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl && \
    curl -fsSLO https://get.helm.sh/helm-v3.2.1-linux-amd64.tar.gz && \
    tar -zxvf helm-v3.2.1-linux-amd64.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    rm -rf helm-v3.2.1-linux-amd64.tar.gz && \
    curl -fsSLO https://github.com/projectcalico/calicoctl/releases/download/v3.15.0/calicoctl && \
    chmod +x ./calicoctl && \
    mv calicoctl /usr/local/bin/calicoctl && \
    kubectl version --client && \
    helm version --client

CMD ["/bin/bash"]

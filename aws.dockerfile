FROM alpine:3.12.0

RUN set -x && apk add --no-cache \
        curl \
	pwgen \
        python3 \
        py3-pip \
        bash \
	postgresql-client \
        coreutils && \
    pip3 install awscli==1.18.99 && \
    curl -fsSLO https://storage.googleapis.com/kubernetes-release/release/v1.18.2/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl && \
    curl -fsSLO https://get.helm.sh/helm-v3.2.1-linux-amd64.tar.gz && \
    tar -zxvf helm-v3.2.1-linux-amd64.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    rm -rf helm-v3.2.1-linux-amd64.tar.gz && \
    curl -fsSLO https://amazon-eks.s3.us-west-2.amazonaws.com/1.16.8/2020-04-16/bin/linux/amd64/aws-iam-authenticator && \
    chmod +x ./aws-iam-authenticator && \
    mv aws-iam-authenticator /usr/local/bin/aws-iam-authenticator && \
    curl -fsSLO https://github.com/projectcalico/calicoctl/releases/download/v3.15.0/calicoctl && \
    chmod +x ./calicoctl && \
    mv calicoctl /usr/local/bin/calicoctl && \
    aws --version && \
    kubectl version --client && \
    helm version --client && \
    aws-iam-authenticator version

CMD ["/bin/bash"]
